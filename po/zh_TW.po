# Chinese translation for gnome-shell-extension-openweather
# gnome-shell-extension-openweather 軟體的正體中文翻譯.
# Copyright (C) 2011
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# bill <bill_zt@sina.com>, 2011.
# shlinux <lishaohui.qd@163.com>, 2014, 2015, 2016, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-shell-extension-openweather\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-09 14:39-0300\n"
"PO-Revision-Date: 2017-03-06 23:22+0800\n"
"Last-Translator: P.-H. Lin <Cypresslin@users.noreply.github.com>\n"
"Language-Team: Chinese (Traditional) <>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#: src/extension.js:171
msgid "..."
msgstr "..."

#: src/extension.js:323
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""
"Openweathermap.org 在沒有 API 金鑰的情況下無法工作。\n"
"請開啓使用擴充套件的預設金鑰，或者到 https://openweathermap.org/appid 註冊，"
"並將您的個人金鑰貼到選項對話框中。"

#: src/extension.js:434 src/extension.js:446
#, javascript-format
msgid "Can not connect to %s"
msgstr "無法連線至 %s"

#: src/extension.js:737 src/weather-settings.ui:302
msgid "Locations"
msgstr "地點"

#: src/extension.js:741
msgid "Reload Weather Information"
msgstr "重新載入天氣資訊"

#: src/extension.js:745
#, fuzzy
msgid "Weather data by:"
msgstr "天氣資料來源："

#: src/extension.js:748
msgid "Weather Settings"
msgstr "天氣設定"

#: src/extension.js:771
#, javascript-format
msgid "Can not open %s"
msgstr "無法打開 %s"

#: src/extension.js:819 src/prefs.js:1072
msgid "Invalid city"
msgstr "無效的城市代碼"

#: src/extension.js:830
msgid "Invalid location! Please try to recreate it."
msgstr "無效地點！請嘗試重建一個。"

#: src/extension.js:870 src/weather-settings.ui:574
msgid "°F"
msgstr "°F"

#: src/extension.js:872 src/weather-settings.ui:575
msgid "K"
msgstr "K"

#: src/extension.js:874 src/weather-settings.ui:576
msgid "°Ra"
msgstr "°Ra"

#: src/extension.js:876 src/weather-settings.ui:577
msgid "°Ré"
msgstr "°Ré"

#: src/extension.js:878 src/weather-settings.ui:578
msgid "°Rø"
msgstr "°Rø"

#: src/extension.js:880 src/weather-settings.ui:579
msgid "°De"
msgstr "°De"

#: src/extension.js:882 src/weather-settings.ui:580
msgid "°N"
msgstr "°N"

#: src/extension.js:884 src/weather-settings.ui:573
msgid "°C"
msgstr "°C"

#: src/extension.js:921
msgid "Calm"
msgstr "無風"

#: src/extension.js:924
msgid "Light air"
msgstr "軟風"

#: src/extension.js:927
msgid "Light breeze"
msgstr "輕風"

#: src/extension.js:930
msgid "Gentle breeze"
msgstr "微風"

#: src/extension.js:933
msgid "Moderate breeze"
msgstr "和風"

#: src/extension.js:936
msgid "Fresh breeze"
msgstr "清風"

#: src/extension.js:939
msgid "Strong breeze"
msgstr "強風"

#: src/extension.js:942
msgid "Moderate gale"
msgstr "疾風"

#: src/extension.js:945
msgid "Fresh gale"
msgstr "大風"

#: src/extension.js:948
msgid "Strong gale"
msgstr "烈風"

#: src/extension.js:951
msgid "Storm"
msgstr "狂風"

#: src/extension.js:954
msgid "Violent storm"
msgstr "暴風"

#: src/extension.js:957
msgid "Hurricane"
msgstr "颶風"

#: src/extension.js:961
msgid "Sunday"
msgstr "星期日"

#: src/extension.js:961
msgid "Monday"
msgstr "星期一"

#: src/extension.js:961
msgid "Tuesday"
msgstr "星期二"

#: src/extension.js:961
msgid "Wednesday"
msgstr "星期三"

#: src/extension.js:961
msgid "Thursday"
msgstr "星期四"

#: src/extension.js:961
msgid "Friday"
msgstr "星期五"

#: src/extension.js:961
msgid "Saturday"
msgstr "星期六"

#: src/extension.js:967
msgid "N"
msgstr "北"

#: src/extension.js:967
msgid "NE"
msgstr "東北"

#: src/extension.js:967
msgid "E"
msgstr "東"

#: src/extension.js:967
msgid "SE"
msgstr "東南"

#: src/extension.js:967
msgid "S"
msgstr "南"

#: src/extension.js:967
msgid "SW"
msgstr "西南"

#: src/extension.js:967
msgid "W"
msgstr "西"

#: src/extension.js:967
msgid "NW"
msgstr "西北"

#: src/extension.js:1020 src/extension.js:1029 src/weather-settings.ui:607
msgid "hPa"
msgstr "hPa"

#: src/extension.js:1024 src/weather-settings.ui:608
msgid "inHg"
msgstr "inHg"

#: src/extension.js:1034 src/weather-settings.ui:609
msgid "bar"
msgstr "bar"

#: src/extension.js:1039 src/weather-settings.ui:610
msgid "Pa"
msgstr "Pa"

#: src/extension.js:1044 src/weather-settings.ui:611
msgid "kPa"
msgstr "kPa"

#: src/extension.js:1049 src/weather-settings.ui:612
msgid "atm"
msgstr "atm"

#: src/extension.js:1054 src/weather-settings.ui:613
msgid "at"
msgstr "at"

#: src/extension.js:1059 src/weather-settings.ui:614
msgid "Torr"
msgstr "Torr"

#: src/extension.js:1064 src/weather-settings.ui:615
msgid "psi"
msgstr "PSI"

#: src/extension.js:1069 src/weather-settings.ui:616
msgid "mmHg"
msgstr "mmHg"

#: src/extension.js:1074 src/weather-settings.ui:617
msgid "mbar"
msgstr "mbar"

#: src/extension.js:1118 src/weather-settings.ui:593
msgid "m/s"
msgstr "m/s"

#: src/extension.js:1122 src/weather-settings.ui:592
msgid "mph"
msgstr "mph"

#: src/extension.js:1127 src/weather-settings.ui:591
msgid "km/h"
msgstr "km/h"

#: src/extension.js:1136 src/weather-settings.ui:594
msgid "kn"
msgstr "kn"

#: src/extension.js:1141 src/weather-settings.ui:595
msgid "ft/s"
msgstr "ft/s"

#: src/extension.js:1228
msgid "Loading ..."
msgstr "載入中 ..."

#: src/extension.js:1232
msgid "Please wait"
msgstr "請稍候"

#: src/extension.js:1303
msgid "Feels Like:"
msgstr ""

#: src/extension.js:1307
msgid "Humidity:"
msgstr "濕度："

#: src/extension.js:1311
msgid "Pressure:"
msgstr "氣壓："

#: src/extension.js:1315
msgid "Wind:"
msgstr "風："

#: src/extension.js:1319
msgid "Gusts:"
msgstr ""

#: src/extension.js:1450
msgid "Tomorrow's Forecast"
msgstr ""

#: src/extension.js:1452
msgid "Day Forecast"
msgstr ""

#: src/openweathermap_org.js:130
msgid "Thunderstorm with light rain"
msgstr "雷暴伴有小雨"

#: src/openweathermap_org.js:132
msgid "Thunderstorm with rain"
msgstr "雷暴伴有雨"

#: src/openweathermap_org.js:134
msgid "Thunderstorm with heavy rain"
msgstr "雷暴伴有大雨"

#: src/openweathermap_org.js:136
msgid "Light thunderstorm"
msgstr "小雷暴"

#: src/openweathermap_org.js:138
msgid "Thunderstorm"
msgstr "雷暴"

#: src/openweathermap_org.js:140
msgid "Heavy thunderstorm"
msgstr "強雷暴"

#: src/openweathermap_org.js:142
msgid "Ragged thunderstorm"
msgstr "局部性雷暴"

#: src/openweathermap_org.js:144
msgid "Thunderstorm with light drizzle"
msgstr "雷暴伴有小毛雨"

#: src/openweathermap_org.js:146
msgid "Thunderstorm with drizzle"
msgstr "雷暴伴有毛雨"

#: src/openweathermap_org.js:148
msgid "Thunderstorm with heavy drizzle"
msgstr "雷暴伴有強毛雨"

#: src/openweathermap_org.js:150
msgid "Light intensity drizzle"
msgstr "小毛雨"

#: src/openweathermap_org.js:152
msgid "Drizzle"
msgstr "毛雨"

#: src/openweathermap_org.js:154
msgid "Heavy intensity drizzle"
msgstr "強毛雨"

#: src/openweathermap_org.js:156
msgid "Light intensity drizzle rain"
msgstr "小毛雨至雨"

#: src/openweathermap_org.js:158
msgid "Drizzle rain"
msgstr "毛雨至雨"

#: src/openweathermap_org.js:160
msgid "Heavy intensity drizzle rain"
msgstr "强毛雨至雨"

#: src/openweathermap_org.js:162
msgid "Shower rain and drizzle"
msgstr "陣雨及毛雨"

#: src/openweathermap_org.js:164
msgid "Heavy shower rain and drizzle"
msgstr "強陣雨及毛雨"

#: src/openweathermap_org.js:166
msgid "Shower drizzle"
msgstr "陣毛雨"

#: src/openweathermap_org.js:168
msgid "Light rain"
msgstr "小雨"

#: src/openweathermap_org.js:170
msgid "Moderate rain"
msgstr "中雨"

#: src/openweathermap_org.js:172
msgid "Heavy intensity rain"
msgstr "大雨"

#: src/openweathermap_org.js:174
msgid "Very heavy rain"
msgstr "豪雨"

#: src/openweathermap_org.js:176
msgid "Extreme rain"
msgstr "大豪雨"

#: src/openweathermap_org.js:178
msgid "Freezing rain"
msgstr "凍雨"

#: src/openweathermap_org.js:180
msgid "Light intensity shower rain"
msgstr "小陣雨"

#: src/openweathermap_org.js:182
msgid "Shower rain"
msgstr "陣雨"

#: src/openweathermap_org.js:184
msgid "Heavy intensity shower rain"
msgstr "強陣雨"

#: src/openweathermap_org.js:186
msgid "Ragged shower rain"
msgstr "局部性陣雨"

#: src/openweathermap_org.js:188
msgid "Light snow"
msgstr "小雪"

#: src/openweathermap_org.js:190
msgid "Snow"
msgstr "雪"

#: src/openweathermap_org.js:192
msgid "Heavy snow"
msgstr "大雪"

#: src/openweathermap_org.js:194
msgid "Sleet"
msgstr "冰雹"

#: src/openweathermap_org.js:196
msgid "Shower sleet"
msgstr "陣雹"

#: src/openweathermap_org.js:198
msgid "Light rain and snow"
msgstr "小雨夾雪"

#: src/openweathermap_org.js:200
msgid "Rain and snow"
msgstr "雨夾雪"

#: src/openweathermap_org.js:202
msgid "Light shower snow"
msgstr "小陣雪"

#: src/openweathermap_org.js:204
msgid "Shower snow"
msgstr "陣雪"

#: src/openweathermap_org.js:206
msgid "Heavy shower snow"
msgstr "強陣雪"

#: src/openweathermap_org.js:208
msgid "Mist"
msgstr "薄霧"

#: src/openweathermap_org.js:210
msgid "Smoke"
msgstr "煙"

#: src/openweathermap_org.js:212
msgid "Haze"
msgstr "霾"

#: src/openweathermap_org.js:214
msgid "Sand/Dust Whirls"
msgstr "沙塵暴"

#: src/openweathermap_org.js:216
msgid "Fog"
msgstr "霧"

#: src/openweathermap_org.js:218
msgid "Sand"
msgstr "沙"

#: src/openweathermap_org.js:220
msgid "Dust"
msgstr "塵"

#: src/openweathermap_org.js:222
msgid "VOLCANIC ASH"
msgstr "火山灰"

#: src/openweathermap_org.js:224
msgid "SQUALLS"
msgstr "颮"

#: src/openweathermap_org.js:226
msgid "TORNADO"
msgstr "龍捲風"

#: src/openweathermap_org.js:228
msgid "Sky is clear"
msgstr "晴朗"

#: src/openweathermap_org.js:230
msgid "Few clouds"
msgstr "少雲"

#: src/openweathermap_org.js:232
msgid "Scattered clouds"
msgstr "疏雲"

#: src/openweathermap_org.js:234
msgid "Broken clouds"
msgstr "碎雲"

#: src/openweathermap_org.js:236
msgid "Overcast clouds"
msgstr "陰天"

#: src/openweathermap_org.js:238
msgid "Not available"
msgstr "無法使用"

#: src/openweathermap_org.js:421
msgid "Yesterday"
msgstr "昨天"

#: src/openweathermap_org.js:423
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d 天以前"

#: src/openweathermap_org.js:438 src/openweathermap_org.js:440
msgid ", "
msgstr "，"

#: src/openweathermap_org.js:456
msgid "?"
msgstr ""

#: src/openweathermap_org.js:523
msgid "Tomorrow"
msgstr "明天"

#: src/prefs.js:189
#, fuzzy
msgid "Searching ..."
msgstr "載入中 ..."

#: src/prefs.js:201 src/prefs.js:239 src/prefs.js:270 src/prefs.js:274
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "搜尋「%s」時遇到無效資料"

#: src/prefs.js:206 src/prefs.js:246 src/prefs.js:277
#, javascript-format
msgid "\"%s\" not found"
msgstr "未找到「%s」"

#: src/prefs.js:224
#, fuzzy
msgid "You need an AppKey to search on openmapquest."
msgstr "developer.mapquest.com 的個人應用金鑰"

#: src/prefs.js:225
#, fuzzy
msgid "Please visit https://developer.mapquest.com/ ."
msgstr "developer.mapquest.com 的個人應用金鑰"

#: src/prefs.js:240
#, fuzzy
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr "developer.mapquest.com 的個人應用金鑰"

#: src/prefs.js:241
#, fuzzy
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr "developer.mapquest.com 的個人應用金鑰"

#: src/prefs.js:323
msgid "Location"
msgstr "地點"

#: src/prefs.js:334
msgid "Provider"
msgstr "資料來源"

#: src/prefs.js:344
msgid "Result"
msgstr ""

#: src/prefs.js:548
#, javascript-format
msgid "Remove %s ?"
msgstr "移除 %s ？"

#: src/prefs.js:561
#, fuzzy
msgid "No"
msgstr "北"

#: src/prefs.js:562
msgid "Yes"
msgstr ""

#: src/prefs.js:1105
msgid "default"
msgstr "預設"

#: src/weather-settings.ui:29
msgid "Edit name"
msgstr "編輯名稱"

#: src/weather-settings.ui:40 src/weather-settings.ui:56
#: src/weather-settings.ui:183
msgid "Clear entry"
msgstr "清除條目"

#: src/weather-settings.ui:47
msgid "Edit coordinates"
msgstr "編輯座標"

#: src/weather-settings.ui:80 src/weather-settings.ui:216
msgid "Cancel"
msgstr "取消"

#: src/weather-settings.ui:90 src/weather-settings.ui:226
msgid "Save"
msgstr "儲存"

#: src/weather-settings.ui:124
msgid "Search Results"
msgstr ""

#: src/weather-settings.ui:161
#, fuzzy
msgid "Search By Location Or Coordinates"
msgstr "透過地點或座標搜尋"

#: src/weather-settings.ui:184
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "例如，「臺北」或者「25.08571815,121.5643773」"

#: src/weather-settings.ui:189
msgid "Find"
msgstr "尋找"

#: src/weather-settings.ui:328
#, fuzzy
msgid "Choose default weather provider"
msgstr "選擇預設天氣資料來源"

#: src/weather-settings.ui:339
msgid "Personal Api key from openweathermap.org"
msgstr "openweathermap.org 的個人 API 金鑰"

#: src/weather-settings.ui:371
msgid "Refresh timeout for current weather [min]"
msgstr "更新目前天氣的逾時設定 [分鐘]"

#: src/weather-settings.ui:383
msgid "Refresh timeout for weather forecast [min]"
msgstr "更新天氣預報的逾時設定 [分鐘]"

#: src/weather-settings.ui:428
msgid "Use extensions api-key for openweathermap.org"
msgstr "使用本擴充套件的 openweathermap.org API 金鑰"

#: src/weather-settings.ui:437
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""
"如果您自己擁有 openweathermap.org 的 API 金鑰，請將其輸入到下面的文本框中，並"
"關閉本選項。"

#: src/weather-settings.ui:451
msgid "Weather provider"
msgstr "天氣資料來源"

#: src/weather-settings.ui:475
#, fuzzy
msgid "Choose geolocation provider"
msgstr "選擇地理位置資料來源"

#: src/weather-settings.ui:497
msgid "Personal AppKey from developer.mapquest.com"
msgstr "developer.mapquest.com 的個人應用金鑰"

#: src/weather-settings.ui:521
msgid "Geolocation provider"
msgstr "地理位置資料來源"

#: src/weather-settings.ui:545
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:62
msgid "Temperature Unit"
msgstr "溫度單位"

#: src/weather-settings.ui:554
msgid "Wind Speed Unit"
msgstr "風速單位"

#: src/weather-settings.ui:563
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:66
msgid "Pressure Unit"
msgstr "氣壓單位"

#: src/weather-settings.ui:596
msgid "Beaufort"
msgstr "蒲福風級"

#: src/weather-settings.ui:631
msgid "Units"
msgstr "單位"

#: src/weather-settings.ui:655
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:116
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:120
msgid "Position in Panel"
msgstr "面板上的位置"

#: src/weather-settings.ui:664
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr "選單框的位置，從左至右，0到100"

#: src/weather-settings.ui:673
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:75
msgid "Wind Direction by Arrows"
msgstr "使用箭頭表示風向"

#: src/weather-settings.ui:682
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:88
msgid "Translate Conditions"
msgstr "翻譯天氣狀態"

#: src/weather-settings.ui:691
#, fuzzy
msgid "Use System Icon Theme"
msgstr "顯示象徵性符號"

#: src/weather-settings.ui:700
msgid "Text on buttons"
msgstr "按鈕上顯示文字"

#: src/weather-settings.ui:709
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:100
msgid "Temperature in Panel"
msgstr "面板上顯示溫度"

#: src/weather-settings.ui:718
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:104
msgid "Conditions in Panel"
msgstr "面板上顯示天氣狀態"

#: src/weather-settings.ui:727
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:108
msgid "Disable Forecast"
msgstr ""

#: src/weather-settings.ui:736
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:112
msgid "Conditions in Forecast"
msgstr "顯示預報天氣狀態"

#: src/weather-settings.ui:745
msgid "Center forecast"
msgstr "預報資訊置中顯示"

#: src/weather-settings.ui:754
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:144
msgid "Number of days in forecast"
msgstr "預報的天數"

#: src/weather-settings.ui:763
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:148
msgid "Maximal number of digits after the decimal point"
msgstr "小數點後的最大位數"

#: src/weather-settings.ui:777
msgid "Center"
msgstr "置中"

#: src/weather-settings.ui:778
msgid "Right"
msgstr "靠右"

#: src/weather-settings.ui:779
msgid "Left"
msgstr "靠左"

#: src/weather-settings.ui:926
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:132
msgid "Maximal length of the location text"
msgstr ""

#: src/weather-settings.ui:950
msgid "Layout"
msgstr "配置"

#: src/weather-settings.ui:985
msgid "Version: "
msgstr "版本："

#: src/weather-settings.ui:999
msgid ""
"<span>Display weather information for any location on Earth in the GNOME "
"Shell</span>"
msgstr ""

#: src/weather-settings.ui:1013
msgid ""
"Weather data provided by: <a href=\"https://openweathermap.org/"
"\">OpenWeatherMap</a>"
msgstr ""

#: src/weather-settings.ui:1026
#, fuzzy
msgid "Maintained by:"
msgstr "維護者："

#: src/weather-settings.ui:1061
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">本程式不提供任何擔保。\n"
"參閱 <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html\">GNU "
"通用公共授權條款，第二版或更高版本</a>以了解詳細信息。</span>"

#: src/weather-settings.ui:1072
msgid "About"
msgstr "關於"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:54
msgid "Weather Provider"
msgstr "天氣資料來源"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:58
msgid "Geolocation Provider"
msgstr "地理位置資料來源"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:70
msgid "Wind Speed Units"
msgstr "風速單位"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr "選擇風速單位。允許 kph，mph，m/s，knots，ft/s 或 Beaufort。"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Choose whether to display wind direction through arrows or letters."
msgstr "選擇採用箭頭還是字母來顯示風向。"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:80
msgid "City to be displayed"
msgstr "要顯示的城市"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:84
msgid "Actual City"
msgstr "實際城市"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:92
#, fuzzy
msgid "System Icons"
msgstr "顯示象徵性符號"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:96
msgid "Use text on buttons in menu"
msgstr "在選單的按鈕上使用文字"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:124
msgid "Horizontal position of menu-box."
msgstr "選單的水平位置。"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:128
msgid "Refresh interval (actual weather)"
msgstr "更新間隔（實際天氣）"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:136
msgid "Refresh interval (forecast)"
msgstr "更新間隔（預報資訊）"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:140
msgid "Center forecastbox."
msgstr "將預報框置中顯示。"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:152
msgid "Your personal API key from openweathermap.org"
msgstr "openweathermap.org 的個人 API 金鑰"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:156
msgid "Use the extensions default API key from openweathermap.org"
msgstr "使用本擴充套件的 openweathermap.org API 金鑰"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:160
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "developer.mapquest.com 的個人應用金鑰"

#~ msgid "unknown (self-build ?)"
#~ msgstr "未知（自建？）"

#~ msgid ""
#~ "Dark Sky does not work without an api-key.\n"
#~ "Please register at https://darksky.net/dev/register and paste your "
#~ "personal key into the preferences dialog."
#~ msgstr ""
#~ "Dark Sky 在沒有 API 金鑰的情況下無法工作。\n"
#~ "請到 https://darksky.net/dev/register 註冊，並將您的個人金鑰貼到選項對話框"
#~ "中。"

#~ msgid "Cloudiness:"
#~ msgstr "雲量："

#~ msgid "Today"
#~ msgstr "今天"

#, javascript-format
#~ msgid "In %d day"
#~ msgid_plural "In %d days"
#~ msgstr[0] "未來 %d 天"

#~ msgid "Extensions default weather provider"
#~ msgstr "擴充套件的預設天氣資料來源"

#~ msgid "Personal Api key from Dark Sky"
#~ msgstr "Dark Sky 的個人 API 金鑰"

#~ msgid ""
#~ "Note: the forecast-timout is not used for Dark Sky, because they do not "
#~ "provide seperate downloads for current weather and forecasts."
#~ msgstr ""
#~ "注意：預報逾時設定不適用於 Dark Sky，因爲他們不提供目前天氣和預報的分別下"
#~ "載。"

#~ msgid ""
#~ "<span>Weather extension to display weather information from <a href="
#~ "\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
#~ "darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
#~ msgstr ""
#~ "<span>爲全世界幾乎所有地點顯示來自 <a href=\"https://openweathermap.org/"
#~ "\">Openweathermap</a> 或 <a href=\"https://darksky.net\">Dark Sky</a> 的天"
#~ "氣資訊的擴充套件。</span>"

#~ msgid "Webpage"
#~ msgstr "網站主頁"

#~ msgid "Your personal API key from Dark Sky"
#~ msgstr "Dark Sky 的個人 API 金鑰"
