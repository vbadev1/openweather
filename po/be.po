# Belarusian translation for gnome-shell-extension-openweather
# Copyright (C) 2018
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# Piatrouski Aliaksei <petrovsky.lexey@gmail.com>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: 0.4\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-09 14:39-0300\n"
"PO-Revision-Date: 2018-10-14 20:57+0300\n"
"Last-Translator: Aliaksei <petrovsky.lexey@gmail.com>\n"
"Language-Team: \n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 2.2\n"

#: src/extension.js:171
msgid "..."
msgstr "..."

#: src/extension.js:323
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""
"Openweathermap.org не працуе без API ключа.\n"
"Уключыце выкарыстоўванне ключа ў дыялогу дадатку, або зарэгіструйцеся на "
"https://openweathermap.org/appid і ўстаўце асабісты ключ у дыялогу настроек."

#: src/extension.js:434 src/extension.js:446
#, javascript-format
msgid "Can not connect to %s"
msgstr "Не атрымліваецца злучыцца з %s"

#: src/extension.js:737 src/weather-settings.ui:302
msgid "Locations"
msgstr "Гарады"

#: src/extension.js:741
msgid "Reload Weather Information"
msgstr "Аднавіць інфармацыю аб надвор'і"

#: src/extension.js:745
#, fuzzy
msgid "Weather data by:"
msgstr "Інфармацыя аб надвор'і прадстаўлена:"

#: src/extension.js:748
msgid "Weather Settings"
msgstr "Налады аплета надвор'я"

#: src/extension.js:771
#, javascript-format
msgid "Can not open %s"
msgstr "Немагчыма адкрыць %s"

#: src/extension.js:819 src/prefs.js:1072
msgid "Invalid city"
msgstr "Няправільны горад"

#: src/extension.js:830
msgid "Invalid location! Please try to recreate it."
msgstr "Няправільнае месцазнаходжанне! Паспрабуйце стварыць яго нанова."

#: src/extension.js:870 src/weather-settings.ui:574
msgid "°F"
msgstr "°F"

#: src/extension.js:872 src/weather-settings.ui:575
msgid "K"
msgstr "K"

#: src/extension.js:874 src/weather-settings.ui:576
msgid "°Ra"
msgstr "°Ra"

#: src/extension.js:876 src/weather-settings.ui:577
msgid "°Ré"
msgstr "°Ré"

#: src/extension.js:878 src/weather-settings.ui:578
msgid "°Rø"
msgstr "°Rø"

#: src/extension.js:880 src/weather-settings.ui:579
msgid "°De"
msgstr "°De"

#: src/extension.js:882 src/weather-settings.ui:580
msgid "°N"
msgstr "°N"

#: src/extension.js:884 src/weather-settings.ui:573
msgid "°C"
msgstr "°C"

#: src/extension.js:921
msgid "Calm"
msgstr "Штыль"

#: src/extension.js:924
msgid "Light air"
msgstr "Ціхі вецер"

#: src/extension.js:927
msgid "Light breeze"
msgstr "Лёгкі вецер"

#: src/extension.js:930
msgid "Gentle breeze"
msgstr "Слабы вецер"

#: src/extension.js:933
msgid "Moderate breeze"
msgstr "Сярэдні вецер"

#: src/extension.js:936
msgid "Fresh breeze"
msgstr "Свежы вецер"

#: src/extension.js:939
msgid "Strong breeze"
msgstr "Моцны вецер"

#: src/extension.js:942
msgid "Moderate gale"
msgstr "Сярэдні вецер"

#: src/extension.js:945
msgid "Fresh gale"
msgstr "Вельмі моцны вецер"

#: src/extension.js:948
msgid "Strong gale"
msgstr "Шторм"

#: src/extension.js:951
msgid "Storm"
msgstr "Моцны шторм"

#: src/extension.js:954
msgid "Violent storm"
msgstr "Вельмі моцны шторм"

#: src/extension.js:957
msgid "Hurricane"
msgstr "Ураган"

#: src/extension.js:961
msgid "Sunday"
msgstr "Нядзеля"

#: src/extension.js:961
msgid "Monday"
msgstr "Панядзелак"

#: src/extension.js:961
msgid "Tuesday"
msgstr "Аўторак"

#: src/extension.js:961
msgid "Wednesday"
msgstr "Серада"

#: src/extension.js:961
msgid "Thursday"
msgstr "Чацвер"

#: src/extension.js:961
msgid "Friday"
msgstr "Пятніца"

#: src/extension.js:961
msgid "Saturday"
msgstr "Субота"

#: src/extension.js:967
msgid "N"
msgstr "С"

#: src/extension.js:967
msgid "NE"
msgstr "СУ"

#: src/extension.js:967
msgid "E"
msgstr "У"

#: src/extension.js:967
msgid "SE"
msgstr "ПдУ"

#: src/extension.js:967
msgid "S"
msgstr "Пд"

#: src/extension.js:967
msgid "SW"
msgstr "ПдЗ"

#: src/extension.js:967
msgid "W"
msgstr "З"

#: src/extension.js:967
msgid "NW"
msgstr "ПнЗ"

#: src/extension.js:1020 src/extension.js:1029 src/weather-settings.ui:607
msgid "hPa"
msgstr "гПа"

#: src/extension.js:1024 src/weather-settings.ui:608
msgid "inHg"
msgstr "дзюйм рт.сл."

#: src/extension.js:1034 src/weather-settings.ui:609
msgid "bar"
msgstr "бар"

#: src/extension.js:1039 src/weather-settings.ui:610
msgid "Pa"
msgstr "Па"

#: src/extension.js:1044 src/weather-settings.ui:611
msgid "kPa"
msgstr "кПа"

#: src/extension.js:1049 src/weather-settings.ui:612
msgid "atm"
msgstr "атм"

#: src/extension.js:1054 src/weather-settings.ui:613
msgid "at"
msgstr "ат"

#: src/extension.js:1059 src/weather-settings.ui:614
msgid "Torr"
msgstr "Торр"

#: src/extension.js:1064 src/weather-settings.ui:615
msgid "psi"
msgstr "psi"

#: src/extension.js:1069 src/weather-settings.ui:616
msgid "mmHg"
msgstr "мм рт.сл."

#: src/extension.js:1074 src/weather-settings.ui:617
msgid "mbar"
msgstr "мбар"

#: src/extension.js:1118 src/weather-settings.ui:593
msgid "m/s"
msgstr "м/с"

#: src/extension.js:1122 src/weather-settings.ui:592
msgid "mph"
msgstr "mph"

#: src/extension.js:1127 src/weather-settings.ui:591
msgid "km/h"
msgstr "км/г"

#: src/extension.js:1136 src/weather-settings.ui:594
msgid "kn"
msgstr "уз"

#: src/extension.js:1141 src/weather-settings.ui:595
msgid "ft/s"
msgstr "фут/с"

#: src/extension.js:1228
msgid "Loading ..."
msgstr "Загрузка ..."

#: src/extension.js:1232
msgid "Please wait"
msgstr "Пачакайце, калі ласка "

#: src/extension.js:1303
msgid "Feels Like:"
msgstr ""

#: src/extension.js:1307
msgid "Humidity:"
msgstr "Вільготнасць:"

#: src/extension.js:1311
msgid "Pressure:"
msgstr "Ціск:"

#: src/extension.js:1315
msgid "Wind:"
msgstr "Вецер:"

#: src/extension.js:1319
msgid "Gusts:"
msgstr ""

#: src/extension.js:1450
msgid "Tomorrow's Forecast"
msgstr ""

#: src/extension.js:1452
msgid "Day Forecast"
msgstr ""

#: src/openweathermap_org.js:130
msgid "Thunderstorm with light rain"
msgstr "Навальніца з невялікім дажджом"

#: src/openweathermap_org.js:132
msgid "Thunderstorm with rain"
msgstr "Навальніца з дажджом"

#: src/openweathermap_org.js:134
msgid "Thunderstorm with heavy rain"
msgstr "Навальніца з моцным дажджом"

#: src/openweathermap_org.js:136
msgid "Light thunderstorm"
msgstr "Невялікая навальніца"

#: src/openweathermap_org.js:138
msgid "Thunderstorm"
msgstr "Навальніца"

#: src/openweathermap_org.js:140
msgid "Heavy thunderstorm"
msgstr "Моцная навальніца"

#: src/openweathermap_org.js:142
msgid "Ragged thunderstorm"
msgstr "Шалёная навальніца"

#: src/openweathermap_org.js:144
msgid "Thunderstorm with light drizzle"
msgstr "Навальніца і невялікая імжа"

#: src/openweathermap_org.js:146
msgid "Thunderstorm with drizzle"
msgstr "Навальніца і імжа"

#: src/openweathermap_org.js:148
msgid "Thunderstorm with heavy drizzle"
msgstr "Навальніца і моцная імжа"

#: src/openweathermap_org.js:150
msgid "Light intensity drizzle"
msgstr "Мерная імжа"

#: src/openweathermap_org.js:152
msgid "Drizzle"
msgstr "Імжа"

#: src/openweathermap_org.js:154
msgid "Heavy intensity drizzle"
msgstr "Інтэнсіўная імжа"

#: src/openweathermap_org.js:156
msgid "Light intensity drizzle rain"
msgstr "Малы дождж"

#: src/openweathermap_org.js:158
msgid "Drizzle rain"
msgstr "Дробны дождж"

#: src/openweathermap_org.js:160
msgid "Heavy intensity drizzle rain"
msgstr "Моцны дробны дождж"

#: src/openweathermap_org.js:162
msgid "Shower rain and drizzle"
msgstr "Залева і імжа"

#: src/openweathermap_org.js:164
msgid "Heavy shower rain and drizzle"
msgstr "Моцнае залева і імжа"

#: src/openweathermap_org.js:166
msgid "Shower drizzle"
msgstr "Моцны дробны дождж"

#: src/openweathermap_org.js:168
msgid "Light rain"
msgstr "Слабы дождж"

#: src/openweathermap_org.js:170
msgid "Moderate rain"
msgstr "Мерны дождж"

#: src/openweathermap_org.js:172
msgid "Heavy intensity rain"
msgstr "Моцны дождж"

#: src/openweathermap_org.js:174
msgid "Very heavy rain"
msgstr "Вельмі моцны дождж"

#: src/openweathermap_org.js:176
msgid "Extreme rain"
msgstr "Праліўны дождж"

#: src/openweathermap_org.js:178
msgid "Freezing rain"
msgstr "Град"

#: src/openweathermap_org.js:180
msgid "Light intensity shower rain"
msgstr "Слабае залева"

#: src/openweathermap_org.js:182
msgid "Shower rain"
msgstr "Залева"

#: src/openweathermap_org.js:184
msgid "Heavy intensity shower rain"
msgstr "Моцнае залева"

#: src/openweathermap_org.js:186
msgid "Ragged shower rain"
msgstr "Мясцовы дождж"

#: src/openweathermap_org.js:188
msgid "Light snow"
msgstr "Лёгкі снег"

#: src/openweathermap_org.js:190
msgid "Snow"
msgstr "Снег"

#: src/openweathermap_org.js:192
msgid "Heavy snow"
msgstr "Моцны снег"

#: src/openweathermap_org.js:194
msgid "Sleet"
msgstr "Мокры снег"

#: src/openweathermap_org.js:196
msgid "Shower sleet"
msgstr "Густы мокры снег"

#: src/openweathermap_org.js:198
msgid "Light rain and snow"
msgstr "Снег і слабы дождж"

#: src/openweathermap_org.js:200
msgid "Rain and snow"
msgstr "Снег і дождж"

#: src/openweathermap_org.js:202
msgid "Light shower snow"
msgstr "Небольшой снегопад"

#: src/openweathermap_org.js:204
msgid "Shower snow"
msgstr "Снегапад"

#: src/openweathermap_org.js:206
msgid "Heavy shower snow"
msgstr "Моцны снегапад"

#: src/openweathermap_org.js:208
msgid "Mist"
msgstr "Імгла"

#: src/openweathermap_org.js:210
msgid "Smoke"
msgstr "Дымка"

#: src/openweathermap_org.js:212
msgid "Haze"
msgstr "Смуга"

#: src/openweathermap_org.js:214
msgid "Sand/Dust Whirls"
msgstr "Пясчаныя/Пылавыя Віхуры"

#: src/openweathermap_org.js:216
msgid "Fog"
msgstr "Туман"

#: src/openweathermap_org.js:218
msgid "Sand"
msgstr "Пясок"

#: src/openweathermap_org.js:220
msgid "Dust"
msgstr "Пыл"

#: src/openweathermap_org.js:222
msgid "VOLCANIC ASH"
msgstr "ВУЛКАНІЧНЫ ПОПЕЛ"

#: src/openweathermap_org.js:224
msgid "SQUALLS"
msgstr "ШКВАЛЬНЫ ВЕЦЕР"

#: src/openweathermap_org.js:226
msgid "TORNADO"
msgstr "ТАРНАДА"

#: src/openweathermap_org.js:228
msgid "Sky is clear"
msgstr "Бясхмарна"

#: src/openweathermap_org.js:230
msgid "Few clouds"
msgstr "Малая хмарнасць"

#: src/openweathermap_org.js:232
msgid "Scattered clouds"
msgstr "Рассеяныя хмары"

#: src/openweathermap_org.js:234
msgid "Broken clouds"
msgstr "Хмарна з праясненнямі"

#: src/openweathermap_org.js:236
msgid "Overcast clouds"
msgstr "Суцэльная воблачнасць"

#: src/openweathermap_org.js:238
msgid "Not available"
msgstr "Недаступна"

#: src/openweathermap_org.js:421
msgid "Yesterday"
msgstr "Учора"

#: src/openweathermap_org.js:423
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d дзень таму"
msgstr[1] "%d дні таму"
msgstr[2] "%d дзён таму"

#: src/openweathermap_org.js:438 src/openweathermap_org.js:440
msgid ", "
msgstr ", "

#: src/openweathermap_org.js:456
msgid "?"
msgstr ""

#: src/openweathermap_org.js:523
msgid "Tomorrow"
msgstr "Заўтра"

#: src/prefs.js:189
#, fuzzy
msgid "Searching ..."
msgstr "Загрузка ..."

#: src/prefs.js:201 src/prefs.js:239 src/prefs.js:270 src/prefs.js:274
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "Памылковыя дадзеныя для пошуку \"%s\""

#: src/prefs.js:206 src/prefs.js:246 src/prefs.js:277
#, javascript-format
msgid "\"%s\" not found"
msgstr "\"%s\" не знойдзен"

#: src/prefs.js:224
#, fuzzy
msgid "You need an AppKey to search on openmapquest."
msgstr "Уласны AppKey ад developer.mapquest.com"

#: src/prefs.js:225
#, fuzzy
msgid "Please visit https://developer.mapquest.com/ ."
msgstr "Асабісты AppKey ад developer.mapquest.com"

#: src/prefs.js:240
#, fuzzy
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr "Уласны AppKey ад developer.mapquest.com"

#: src/prefs.js:241
#, fuzzy
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr "Асабісты AppKey ад developer.mapquest.com"

#: src/prefs.js:323
msgid "Location"
msgstr "Горад"

#: src/prefs.js:334
msgid "Provider"
msgstr "Крыніца"

#: src/prefs.js:344
msgid "Result"
msgstr ""

#: src/prefs.js:548
#, javascript-format
msgid "Remove %s ?"
msgstr "Выдаліць %s ?"

#: src/prefs.js:561
#, fuzzy
msgid "No"
msgstr "С"

#: src/prefs.js:562
msgid "Yes"
msgstr ""

#: src/prefs.js:1105
msgid "default"
msgstr "па змоўчанні"

#: src/weather-settings.ui:29
msgid "Edit name"
msgstr "Рэдагаваць імя"

#: src/weather-settings.ui:40 src/weather-settings.ui:56
#: src/weather-settings.ui:183
msgid "Clear entry"
msgstr "Ачысціць запіс"

#: src/weather-settings.ui:47
msgid "Edit coordinates"
msgstr "Рэдагаваць каардынаты"

#: src/weather-settings.ui:80 src/weather-settings.ui:216
msgid "Cancel"
msgstr "Адмяніць"

#: src/weather-settings.ui:90 src/weather-settings.ui:226
msgid "Save"
msgstr "Захаваць"

#: src/weather-settings.ui:124
msgid "Search Results"
msgstr ""

#: src/weather-settings.ui:161
#, fuzzy
msgid "Search By Location Or Coordinates"
msgstr "Пошук па назве або каардынатам"

#: src/weather-settings.ui:184
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "напрыклад: Мінск, Брэст ці -8.5211767,179.1976747"

#: src/weather-settings.ui:189
msgid "Find"
msgstr "Знайсці"

#: src/weather-settings.ui:328
#, fuzzy
msgid "Choose default weather provider"
msgstr "Выбар крыніцы дадзеных па замоўчванні"

#: src/weather-settings.ui:339
msgid "Personal Api key from openweathermap.org"
msgstr "Асабісты API ключ ад openweathermap.org"

#: src/weather-settings.ui:371
msgid "Refresh timeout for current weather [min]"
msgstr "Перыядычнасць абнаўлення бягучага прагнозу надвор'я [хвіл.]"

#: src/weather-settings.ui:383
msgid "Refresh timeout for weather forecast [min]"
msgstr "Перыядычнасць абнаўлення прагнозу надвор'я [хвіл.]"

#: src/weather-settings.ui:428
msgid "Use extensions api-key for openweathermap.org"
msgstr "Выкарыстоўваць API ключ дадатку ад openweathermap.org"

#: src/weather-settings.ui:437
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""
"Адкючыце, калі ў вас ёсць уласны API ключ ад openweathermap.org і ўвядзіце "
"яго у поле нижэй."

#: src/weather-settings.ui:451
msgid "Weather provider"
msgstr "Крыніца звестак аб надвор'і"

#: src/weather-settings.ui:475
#, fuzzy
msgid "Choose geolocation provider"
msgstr "Выбар крыніцы геазвестак"

#: src/weather-settings.ui:497
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Асабісты AppKey ад developer.mapquest.com"

#: src/weather-settings.ui:521
msgid "Geolocation provider"
msgstr "Крыніца геададзеных"

#: src/weather-settings.ui:545
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:62
msgid "Temperature Unit"
msgstr "Адзінкі вымярэння тэмператууры"

#: src/weather-settings.ui:554
msgid "Wind Speed Unit"
msgstr "Адзінкі вымярэння хуткасці ветру"

#: src/weather-settings.ui:563
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:66
msgid "Pressure Unit"
msgstr "Адзінкі вымярэння ціску"

#: src/weather-settings.ui:596
msgid "Beaufort"
msgstr "Па шкале Бафорта"

#: src/weather-settings.ui:631
msgid "Units"
msgstr "Адзінкі вымярэння"

#: src/weather-settings.ui:655
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:116
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:120
msgid "Position in Panel"
msgstr "Пазіцыя на панелі"

#: src/weather-settings.ui:664
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr "Пазіцыя акна ў [%] ад 0 (злева) да 100 (справа)"

#: src/weather-settings.ui:673
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:75
msgid "Wind Direction by Arrows"
msgstr "Паказваць напрамак ветру стрэлкамі"

#: src/weather-settings.ui:682
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:88
msgid "Translate Conditions"
msgstr "Перакладаць умовы надвор'я"

#: src/weather-settings.ui:691
#, fuzzy
msgid "Use System Icon Theme"
msgstr "Выкарыстоўваць сімвалічныя абразкі"

#: src/weather-settings.ui:700
msgid "Text on buttons"
msgstr "Тэкст на кнопках"

#: src/weather-settings.ui:709
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:100
msgid "Temperature in Panel"
msgstr "Паказваць тэмпературу на панэлі"

#: src/weather-settings.ui:718
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:104
msgid "Conditions in Panel"
msgstr "Паказваць умовы надвор'я на панэлі"

#: src/weather-settings.ui:727
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:108
msgid "Disable Forecast"
msgstr ""

#: src/weather-settings.ui:736
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:112
msgid "Conditions in Forecast"
msgstr "Паказваць умовы надвор'я у прагнозе"

#: src/weather-settings.ui:745
msgid "Center forecast"
msgstr "Цэнтраваць прагноз"

#: src/weather-settings.ui:754
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:144
msgid "Number of days in forecast"
msgstr "Колькасць дзён у прагнозе"

#: src/weather-settings.ui:763
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:148
msgid "Maximal number of digits after the decimal point"
msgstr "Колькасць знакаў пасля коскі"

#: src/weather-settings.ui:777
msgid "Center"
msgstr "Па цэнтру"

#: src/weather-settings.ui:778
msgid "Right"
msgstr "Справа"

#: src/weather-settings.ui:779
msgid "Left"
msgstr "Злева"

#: src/weather-settings.ui:926
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:132
msgid "Maximal length of the location text"
msgstr ""

#: src/weather-settings.ui:950
msgid "Layout"
msgstr "Планіроўка"

#: src/weather-settings.ui:985
msgid "Version: "
msgstr "Версія: "

#: src/weather-settings.ui:999
msgid ""
"<span>Display weather information for any location on Earth in the GNOME "
"Shell</span>"
msgstr ""

#: src/weather-settings.ui:1013
msgid ""
"Weather data provided by: <a href=\"https://openweathermap.org/"
"\">OpenWeatherMap</a>"
msgstr ""

#: src/weather-settings.ui:1026
#, fuzzy
msgid "Maintained by:"
msgstr "Аўтар"

#: src/weather-settings.ui:1061
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">Гэтае прыстасаванне пастаўляецца БЕЗ УСЯЛЯКІХ "
"ГАРАНТЫЙ.\n"
"Глядзі <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, версіі 2 ці больш позняй</a> для дэталяў.</"
"span>"

#: src/weather-settings.ui:1072
msgid "About"
msgstr "Пра праграму"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:54
msgid "Weather Provider"
msgstr "Крыніца дадзеных аб надвор'і"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:58
msgid "Geolocation Provider"
msgstr "Крыніца геададзеных"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:70
msgid "Wind Speed Units"
msgstr "Адзінкі вымярэння хуткасці ветру"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""
"Абярыце адзінку вымярэння хуткасці ветру. Дазволеныя значеннні: км/г, mph "
"(мілі у гадзіну), м/с, вузлы, фут/с ці па шкале Бафорта."

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Choose whether to display wind direction through arrows or letters."
msgstr "Абярыце як паказваць напрамкі ветру - стрэлкамі ці літарамі."

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:80
msgid "City to be displayed"
msgstr "Горад для паказу"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:84
msgid "Actual City"
msgstr "Бягучы горад"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:92
#, fuzzy
msgid "System Icons"
msgstr "Выкарыстоўваць сімвалічныя абразкі"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:96
msgid "Use text on buttons in menu"
msgstr "Выкарыстоўваць тэкст на кнопках у меню"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:124
msgid "Horizontal position of menu-box."
msgstr "Гарызантальнае становішча акна."

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:128
msgid "Refresh interval (actual weather)"
msgstr "Интэрвал абнаўлення (бягучае надвор'е)"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:136
msgid "Refresh interval (forecast)"
msgstr "Интэрвал абнаўлення (надвор'е)"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:140
msgid "Center forecastbox."
msgstr "Цэнтраваць прагноз."

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:152
msgid "Your personal API key from openweathermap.org"
msgstr "Асабісты API ключ ад openweathermap.org"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:156
msgid "Use the extensions default API key from openweathermap.org"
msgstr "Выкарыстоўваць API ключ дадатку ад openweathermap.org"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:160
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Уласны AppKey ад developer.mapquest.com"

#~ msgid "unknown (self-build ?)"
#~ msgstr "невядома (уласная зборка?)"

#~ msgid ""
#~ "Dark Sky does not work without an api-key.\n"
#~ "Please register at https://darksky.net/dev/register and paste your "
#~ "personal key into the preferences dialog."
#~ msgstr ""
#~ "Dark Sky не працуе без API ключа.\n"
#~ "Зарэгіструйцеся на https://darksky.net/dev/register і ўстаўце асабісты "
#~ "ключ у дыялогу настроек."

#~ msgid "Cloudiness:"
#~ msgstr "Хмарнасць:"

#~ msgid "Today"
#~ msgstr "Сёння"

#, javascript-format
#~ msgid "In %d day"
#~ msgid_plural "In %d days"
#~ msgstr[0] "На %d дзень"
#~ msgstr[1] "На %d дні"
#~ msgstr[2] "На %d дзён"

#~ msgid "Extensions default weather provider"
#~ msgstr "Крыніца дадзеных аб надвор'і па змоўчанні"

#~ msgid "Personal Api key from Dark Sky"
#~ msgstr "Асабісты API ключ ад  Dark Sky"

#~ msgid ""
#~ "Note: the forecast-timout is not used for Dark Sky, because they do not "
#~ "provide seperate downloads for current weather and forecasts."
#~ msgstr ""
#~ "Заўвага: перыядычнасць абнаўлення прагнозу надвор'я не выкарыстоўваецца "
#~ "для Dark Sky, бо ён не падтрымлівае паасобную загрузку для бягучага "
#~ "прагнозу надвор'я."

#~ msgid ""
#~ "<span>Weather extension to display weather information from <a href="
#~ "\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
#~ "darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
#~ msgstr ""
#~ "<span>Пашырэнне надвор'я ад <a href=\"https://openweathermap.org/"
#~ "\">Openweathermap</a> ці <a href=\"https://darksky.net\">Dark Sky</a> "
#~ "амаль для усіх месцаў у свеце.</span>"

#~ msgid "Webpage"
#~ msgstr "Хатняя старонка"

#~ msgid "Your personal API key from Dark Sky"
#~ msgstr "Асабісты API ключ ад Dark Sky"
